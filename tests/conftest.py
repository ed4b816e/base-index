import os

import pytest


@pytest.fixture(scope="session", autouse=True)
def configure_logging():
    import logging

    for _logger in [
        "pika",
        "cassandra",
    ]:
        c_conn_logger = logging.getLogger(_logger)
        c_conn_logger.setLevel(logging.ERROR)


@pytest.fixture(scope="session", autouse=True)
def set_test_environments(session_mocker):
    session_mocker.patch.dict(
        os.environ,
        {
            "PREFIX": "/index",
            "CASSANDRA_HOST": "localhost",
            "CASSANDRA_PORT": "9042",
            "CASSANDRA_USERNAME": "cassandra",
            "CASSANDRA_PASSWORD": "",
        },
    )


@pytest.fixture(scope="function")
def client():
    from starlette.testclient import TestClient
    from src import create_app

    yield TestClient(create_app())


@pytest.fixture
def cassandra_session():
    from src.models.cassandra.document import DocumentObject
    from cassandra.cqlengine.management import drop_table, sync_table
    from src.models.cassandra.base import get_cluster
    sync_table(DocumentObject)
    with get_cluster() as cluster:
        yield cluster.connect()
    drop_table(DocumentObject)
