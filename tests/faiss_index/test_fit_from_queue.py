import pytest


@pytest.mark.asyncio
async def test_fit_from_queue():
    from src.utils.daemon import get_threaded_loop
    from src.utils.consumer import Consumer
    from src.utils.publisher import Publisher
    import logging
    import asyncio

    publisher = Publisher()

    loop = asyncio.get_running_loop()

    await publisher.setup(loop)

    async def get_message(num):
        _loop = asyncio.get_running_loop()
        consumer = Consumer()
        await consumer.setup(_loop)
        logging.warning(f"started {num}")
        async for message, reply_to, correlation_id in consumer.run():
            logging.warning((num, message))
            await consumer.send_acknowledgment(message, reply_to, correlation_id)
            await asyncio.sleep(1)
        await consumer.payoff()

    threaded_loop = get_threaded_loop()

    asyncio.run_coroutine_threadsafe(get_message(0), threaded_loop)
    asyncio.run_coroutine_threadsafe(get_message(1), threaded_loop)

    await asyncio.sleep(5)

    await publisher.publish({"message": "hello0"})
    logging.warning("published")
    await publisher.publish({"message": "hello1"})
    logging.warning("published")
    await publisher.publish({"message": "hello2"})
    logging.warning("published")
    await publisher.publish({"message": "hello3"})
    logging.warning("published")

    await publisher.payoff()
