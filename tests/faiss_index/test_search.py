import datetime
import logging


def test_search(client, cassandra_session):
    import numpy as np

    paragraphs_num = 2
    embedding_dim = 768
    paragraphs_embedding = np.random.rand(paragraphs_num, embedding_dim).tolist()

    payload = {"documents": []}
    for i in range(2):
        payload["documents"].append(
            {
                "id": f"{i}",
                "date": str(datetime.datetime.now() + datetime.timedelta(hours=i)),
                "domain": "google.com",
                "paragraphs_embedding": paragraphs_embedding,
                "paragraphs_num": list(range(paragraphs_num)),
            }
        )

    for _ in range(1, 3):

        response = client.post("/index/fit", json=payload)

        assert response.status_code == 200

        response_json = response.json()

        assert response_json["success"] is True

        response = client.post("/index/search", json=payload)

        assert response.status_code == 200

        response_json = response.json()

        assert response_json["success"] is True

        logging.warning(response_json)
