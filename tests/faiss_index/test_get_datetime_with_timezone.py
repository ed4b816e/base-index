import logging


def test_get_datetime_with_timezone():
    import datetime
    from src.repositories.faiss_index import FaissIndexRepository

    repo = FaissIndexRepository()

    datetime_utc = datetime.datetime.utcnow()
    datetime_with_timezone = repo.get_datetime_with_timezone(datetime_utc)

    assert datetime_with_timezone is not None
    logging.warning(datetime_with_timezone)

    logging.warning(datetime_with_timezone.subtract(days=3).astimezone())