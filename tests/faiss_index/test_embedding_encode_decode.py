import logging


def test_embedding_encode_decode():
    from src.repositories.faiss_index import FaissIndexRepository
    import numpy as np

    initial_embedding = np.random.randn(10).astype(np.float16)

    logging.warning(initial_embedding)

    embedding_bytes = FaissIndexRepository.encode_embedding(initial_embedding)

    logging.warning(embedding_bytes)

    final_embedding = FaissIndexRepository.decode_embedding(embedding_bytes)

    logging.warning(final_embedding)

    assert final_embedding.shape == initial_embedding.shape

    assert np.array_equal(initial_embedding, final_embedding)