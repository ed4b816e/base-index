import datetime
import json


def test_fit(client, cassandra_session):
    import numpy as np

    for j in range(1, 2):
        payload = {"documents": []}
        for i in range(2):
            payload["documents"].append(
                {
                    "id": f"{j*i}",
                    "date": str(datetime.datetime.now() + datetime.timedelta(hours=j*i)),
                    "domain": "google.com",
                    "paragraphs_embedding": np.random.rand(2, 768).tolist(),
                    "paragraphs_num": [0, 1],
                }
            )

        with open("test.json", "w") as f:
            f.write(json.dumps(payload))

        break

        response = client.post("/index/fit", json=payload)

        assert response.status_code == 200

        response_json = response.json()

        assert response_json["success"] is True
