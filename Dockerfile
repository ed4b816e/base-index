FROM python:3.10-slim

RUN apt-get --yes update &&\
    apt-get --yes install libopenblas-dev libomp-dev build-essential curl git

RUN groupadd --gid 1001 deploy && \
    useradd --uid 1001 --gid deploy --shell /bin/bash --create-home deploy

# RUN mkdir -m=00755 /app
# RUN chown -R deploy:deploy /app

# USER deploy

ENV PATH="${PATH}:${HOME}/.local/bin"

COPY /src /app/src
COPY poetry.lock pyproject.toml README.md /app/

WORKDIR /app

RUN pip install --upgrade pip && pip install setuptools && pip install poetry \
    && poetry build \
    && pip install ./dist/*.tar.gz

CMD ["uvicorn", "--factory", "src:create_app", "--host", "0.0.0.0", "--port", "8000"]
