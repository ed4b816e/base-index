import pendulum

from abc import ABC, abstractmethod

from src.schemas.document import DocumentSchema


class BaseIndexRepository(ABC):
    def __new__(cls):
        if not hasattr(cls, "instance"):
            setattr(cls, "instance", super(BaseIndexRepository, cls).__new__(cls))
        return getattr(cls, "instance")

    @abstractmethod
    def fill_index(self, date: pendulum.DateTime):
        pass

    @abstractmethod
    def search(self, documents: list[DocumentSchema]):
        pass

    @abstractmethod
    def fit(self, documents: list[DocumentSchema]):
        pass
