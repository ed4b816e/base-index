import datetime
import time
from logging import getLogger
from typing import Callable
from typing import Iterable

import faiss
import numpy as np
import pendulum
from cassandra.cqlengine.query import BatchQuery

from .base_index import BaseIndexRepository
from src.models.cassandra import DocumentObject
from src.schemas.document import DocumentSchema
from src.schemas.document_request import DocumentRequest
from src.schemas.document_response import DocumentResponse
from src.schemas.semantic_from import SemanticFrom
from src.settings import index_settings

logger = getLogger(__file__)


class FaissIndexRepository(BaseIndexRepository):
    def __init__(self):
        self.index_window_days: int = index_settings.index_window_days
        self.threshold: float = index_settings.index_threshold
        self.index_dim: int = index_settings.index_dim
        self.return_top_n: int = index_settings.index_return_top_n

        self.index_date: pendulum.Date | None = None
        self.faiss_index: faiss.IndexFlatIP | None = None
        self.documents_index = np.empty(0, dtype=DocumentSchema)

        self.timezone = pendulum.timezone("Europe/Moscow")

    def get_documents(self, date: pendulum.Date) -> Iterable[DocumentObject]:
        for current_day in range(self.index_window_days):
            for hour in range(24):
                date_to_fetch = date
                if current_day:
                    date_to_fetch = date.subtract(days=current_day)
                time_to_fetch = pendulum.datetime(
                    year=date_to_fetch.year,
                    month=date_to_fetch.month,
                    day=date_to_fetch.day,
                    hour=hour,
                )
                # logger.info(f"Fetching paragraphs for binned timestamp partition {time_to_fetch=}")
                paragraph_rows: list[DocumentObject] = DocumentObject.objects(
                    binned_timestamp=time_to_fetch
                )
                yield from paragraph_rows

    def fill_index(self, date: pendulum.DateTime) -> None:
        date = date.date()
        if not self.index_date or self.index_date != date:

            start_time = time.time()
            documents_index = []
            documents_embeddings = []
            for document in self.get_documents(date):
                embedding = self.decode_embedding(document.embedding)
                document_date: pendulum.DateTime = self.get_datetime_with_timezone(document.date)

                documents_index.append(
                    DocumentSchema(
                        id=document.id,
                        date=document_date,
                        domain=document.domain,
                    )
                )
                documents_embeddings.append(embedding)

            self.clean()

            if not np.any(documents_index):
                logger.info("Index is empty")
                return

            self.documents_index = np.array(documents_index, dtype=DocumentSchema)

            documents_embeddings = np.stack(documents_embeddings)
            faiss.normalize_L2(documents_embeddings)

            self.faiss_index.add(documents_embeddings)
            self.index_date = date
            logger.info(f"Index length is {len(self.documents_index)}")
            logger.info(f"Faiss index rebuilt in {time.time() - start_time} seconds")

    def fit(self, documents: list[DocumentRequest]):
        start_time = time.time()

        documents_index = []
        documents_embedding = []

        for document in documents:
            document_date: pendulum.DateTime = self.get_datetime_with_timezone(document.date)
            self.fill_index(document_date)

            paragraphs_embedding = np.array(document.paragraphs_embedding, dtype=np.float32)
            if not np.all(paragraphs_embedding.shape):
                continue

            faiss.normalize_L2(paragraphs_embedding)
            documents_index.extend(
                [
                    DocumentSchema(
                        id=document.id,
                        date=document_date,
                        domain=document.domain,
                    )
                ]
                * len(paragraphs_embedding)
            )
            documents_embedding.extend(paragraphs_embedding)
            time_to_fit = pendulum.datetime(
                year=document_date.year,
                month=document_date.month,
                day=document_date.day,
                hour=document_date.hour,
            )

            with BatchQuery() as batch:
                for num, embedding_list in zip(
                    document.paragraphs_num, document.paragraphs_embedding
                ):
                    embedding_bytes = self.encode_embedding(embedding_list)
                    DocumentObject.batch(batch).create(
                        binned_timestamp=time_to_fit,
                        id=document.id,
                        date=document_date,
                        domain=document.domain,
                        embedding=embedding_bytes,
                        num=num,
                    )

        if len(documents_embedding) > 0:
            self.faiss_index.add(np.stack(documents_embedding))
            self.documents_index = np.concatenate((self.documents_index, documents_index), axis=0)
        logger.info(f"Index length is {len(self.documents_index)}")
        logger.info(f"Faiss index fit in {time.time() - start_time} seconds")

    def search(self, documents: list[DocumentRequest]) -> list[DocumentResponse]:
        start_time = time.time()

        output: list[DocumentResponse] = []
        for document in documents:
            paragraphs_embedding = np.array(document.paragraphs_embedding, dtype=np.float32)
            if not np.all(paragraphs_embedding.shape):
                continue
            document_date: pendulum.DateTime = self.get_datetime_with_timezone(document.date)
            self.fill_index(document_date)
            faiss.normalize_L2(paragraphs_embedding)
            _, dists, indices = self.faiss_index.range_search(paragraphs_embedding, self.threshold)
            if not np.all(indices.shape):
                continue
            dists, indices = np.hstack(dists), np.hstack(indices)

            documents_index = self.documents_index[indices]
            filtered = np.array([d.id for d in documents_index]) != document.id

            if np.any(filtered):
                documents_index = documents_index[filtered]
                dists = dists[filtered]
            else:
                continue

            documents_index, doc_scores = zip(*list(self.group_by(documents_index, dists, np.mean)))
            documents_index = np.array(documents_index)
            doc_scores = np.array(doc_scores)

            filtered = np.argsort(-doc_scores)[: self.return_top_n]

            documents_index = documents_index[filtered]
            doc_scores = doc_scores[filtered]

            document_obj = document.model_dump()
            semantic_from = []
            for doc, doc_score in zip(documents_index, doc_scores):
                doc_date: pendulum.DateTime = self.get_datetime_with_timezone(doc.date)
                if doc_date > document_date:
                    continue
                doc_delta = doc_date.diff(document_date).total_seconds()
                semantic_from.append(
                    SemanticFrom(
                        id=doc.id,
                        ratio=doc_score,
                        delta=doc_delta,
                        domain=doc.domain,
                    )
                )
            document_obj["semantic_from"] = semantic_from
            output.append(DocumentResponse.model_validate(document_obj))

        logger.info(f"Faiss index search in {time.time() - start_time} seconds")
        return output

    @staticmethod
    def encode_embedding(embedding: np.ndarray | list) -> bytes:
        embedding_bytes = np.array(embedding, dtype=np.float16).tobytes()
        return embedding_bytes

    @staticmethod
    def decode_embedding(embedding_bytes: bytes) -> np.ndarray:
        embedding = np.frombuffer(embedding_bytes, dtype=np.float16).astype(np.float32)
        return embedding

    def get_datetime_with_timezone(self, date: datetime.datetime) -> pendulum.DateTime:
        return pendulum.instance(date)

    def clean(self) -> None:
        self.index_date = None
        self.faiss_index = faiss.IndexFlatIP(self.index_dim)
        self.documents_index = np.empty(0, dtype=DocumentSchema)

    @staticmethod
    def group_by(keys: np.ndarray, values: np.ndarray, aggregate_fnc: Callable):
        assert keys.size == values.size
        for key in set(keys):
            yield key, aggregate_fnc(values[keys == key])
