from os import environ
from cassandra.cqlengine import connection
from cassandra.cluster import Cluster

from src.settings import cassandra_settings
from src.constants import KEYSPACE

environ["CQLENG_ALLOW_SCHEMA_MANAGEMENT"] = "1"


def get_cluster() -> Cluster:
    return Cluster(
        contact_points=cassandra_settings.host.split(","),
        port=cassandra_settings.port,
        **cassandra_settings.extra,
    )


def setup_connection():
    cluster = get_cluster()
    with cluster as cluster:
        session = cluster.connect()
        session.execute(
            """
            CREATE KEYSPACE IF NOT EXISTS %s
            WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '1' }
            """ % KEYSPACE
        )
        connection.setup(
            cluster.contact_points, KEYSPACE,
            protocol_version=cluster.protocol_version,
            load_balancing_policy=cluster.load_balancing_policy,
            auth_provider=cluster.auth_provider
        )
