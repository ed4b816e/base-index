from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model

from .base import KEYSPACE


class DocumentObject(Model):
    __table_name__ = "documents"
    __keyspace__ = KEYSPACE

    binned_timestamp = columns.DateTime(primary_key=True, partition_key=True)
    id = columns.Ascii(primary_key=True)
    date = columns.DateTime()
    domain = columns.Text()
    embedding = columns.Blob()
    num = columns.SmallInt(primary_key=True)
