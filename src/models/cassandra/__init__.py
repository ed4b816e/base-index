from cassandra.cqlengine.management import sync_table

from .base import setup_connection
from .document import DocumentObject


setup_connection()
sync_table(DocumentObject)
