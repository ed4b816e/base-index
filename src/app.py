import asyncio
import json
import logging
import os
from pathlib import Path

from fastapi import FastAPI
from fastapi import Request
from fastapi import Response
from fastapi import status
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

from src.api.routers.router import router
from src.constants import PREFIX
from src.repositories.faiss_index import FaissIndexRepository
from src.schemas.document_request import DocumentsRequest
from src.services.faiss_index import IndexService
from src.utils import logger
from src.utils.consumer import Consumer
from src.utils.daemon import get_threaded_loop


VERSION = "0.1.0"


def create_app() -> FastAPI:
    app = FastAPI(
        title=os.environ.get("APP_NAME", "Base Index"),
        debug=False,
        version=VERSION,
        docs_url=f"{PREFIX}/docs",
        openapi_url=f"{PREFIX}/openapi.json",
    )

    logger.CustomizeLogger.make_logger(
        app=app, config_path=Path(os.environ.get("LOGGING_CONFIG", "configs/logging_config.json"))
    )

    app.add_middleware(
        CORSMiddleware,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @app.exception_handler(Exception)
    async def unicorn_exception_handler(request: Request, exc: Exception):
        logging.error(exc)
        return JSONResponse(
            status_code=400,
            content={"message": str(exc)},
        )

    @app.exception_handler(RequestValidationError)
    async def validation_exception_handler(request: Request, exc: RequestValidationError):
        logging.error(exc)
        return JSONResponse(
            status_code=422,
            content={"message": str(exc)},
        )

    @app.on_event("startup")
    async def startup_consumer():
        loop = get_threaded_loop()

        async def setup_consumer():
            faiss_index = IndexService(FaissIndexRepository())
            _loop = asyncio.get_event_loop()
            consumer = Consumer()
            await consumer.setup(_loop)
            async for message in consumer.run():
                try:
                    payload = json.loads(message.body.decode("utf-8"))
                    payload = DocumentsRequest.model_validate(payload)
                    faiss_index.fit(payload.documents)
                    result = {"success": True}
                except Exception as error:
                    logging.warning(error, exc_info=True)
                    result = {"success": False, "message": str(error)}
                await consumer.send_acknowledgment(result, message.reply_to, message.correlation_id)

        asyncio.run_coroutine_threadsafe(setup_consumer(), loop)

    @app.get(
        f"{PREFIX}/",
        response_class=JSONResponse,
        status_code=200,
        description="Запрос проверки состояния сервиса.",
    )
    async def healthcheck_endpoint(response: Response):
        try:
            pass
        except Exception as error:
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {"success": False}
        else:
            return {"success": True}

    app.include_router(router)
    return app
