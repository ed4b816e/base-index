from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class IndexSettings(BaseSettings):
    index_window_days: int = Field(alias="INDEX_WINDOW_DAYS", default=30)
    index_dim: int = Field(alias="INDEX_DIM", default=768)
    index_return_top_n: int = Field(alias="INDEX_RETURN_TOP_N", default=10)
    index_threshold: float = Field(alias="INDEX_THRESHOLD", default=0.9)

    model_config = SettingsConfigDict(env_prefix="INDEX_", env_file=".env", env_file_encoding="utf-8")
