from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class FluentLoggerSettings(BaseSettings):
    host: str = Field(alias="FLUENT_HOST", default="localhost")
    port: int = Field(alias="FLUENT_PORT", default=24224)

    model_config = SettingsConfigDict(env_prefix="FLUENT_", env_file=".env", env_file_encoding="utf-8")
