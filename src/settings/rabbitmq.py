from pydantic_settings import BaseSettings, SettingsConfigDict
from pydantic import Field


class RabbitMQSettings(BaseSettings):
    host: str = Field(alias="RABBITMQ_HOST", default="localhost")
    port: int = Field(alias="RABBITMQ_PORT", default=5672)
    username: str = Field(alias="RABBITMQ_USERNAME", default="guest")
    password: str = Field(alias="RABBITMQ_PASSWORD", default="guest")
    virtualhost: str = Field(alias="RABBITMQ_VIRTUALHOST", default="/")
    routing_key: str = Field(alias="RABBITMQ_ROUTING_KEY", default="queue")
    max_length: int = Field(alias="RABBITMQ_QUEUE_MAX_LENGTH", default=100)
    exchange: str = Field(alias="RABBITMQ_EXCHANGE", default="index.default.exchange")

    def url(self, _async: bool = True) -> str:
        if _async:
            return (
                "ampqs://%(username)s:%(password)s@%(host)s:%(port)d/%(virtualhost)s" % self.model_dump()
            )
        return "amqp://%(username)s:%(password)s@%(host)s:%(port)d/%(virtualhost)s" % self.model_dump()

    def rpc(self) -> str:
        return "rpc://%(username)s:%(password)s@%(host)s:%(port)d/" % self.model_dump()

    model_config = SettingsConfigDict(env_prefix="RABBITMQ_", env_file=".env", env_file_encoding="utf-8")
