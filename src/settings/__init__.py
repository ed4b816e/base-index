from .cassandra import CassandraSettings
from .index import IndexSettings
from .fluent import FluentLoggerSettings
from .rabbitmq import RabbitMQSettings


rabbitmq_settings = RabbitMQSettings()
cassandra_settings = CassandraSettings()
index_settings = IndexSettings()
fluent_settings = FluentLoggerSettings()
