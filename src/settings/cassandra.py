from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class CassandraSettings(BaseSettings):
    host: str = Field(alias='CASSANDRA_HOST', default='cassandra')
    port: int = Field(alias='CASSANDRA_PORT', default=9042)
    username: str = Field(alias='CASSANDRA_USERNAME', default='cassandra')
    password: str = Field(alias='CASSANDRA_PASSWORD', default='')
    extra: dict = Field(
        alias="CASSANDRA_EXTRA",
        default={
            "protocol_version": 5,
            # "load_balancing_policy": "DCAwareRoundRobinPolicy",
            # "load_balancing_policy_args": {
            #     "local_dc": "kubernetes",
            #     "used_hosts_per_remote_dc": "3"
            # }
        },
    )

    model_config = SettingsConfigDict(env_prefix="CASSANDRA_", env_file=".env", env_file_encoding="utf-8")
