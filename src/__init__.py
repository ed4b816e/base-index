from .app import *

__version__ = VERSION


__all__ = [
    "create_app",
    "VERSION",
]
