import pendulum
from logging import getLogger

from src.schemas.document_request import DocumentRequest, DocumentsRequest
from src.schemas.document_response import DocumentResponse
from src.repositories.base_index import BaseIndexRepository

logger = getLogger(__file__)


class IndexService:
    def __init__(self, index_repo: BaseIndexRepository):
        self.index_repo = index_repo

    def fill_index(self, date: pendulum.DateTime | None) -> None:
        self.index_repo.fill_index(date)

    def fit(self, documents: list[DocumentRequest]) -> None:
        self.index_repo.fit(documents)

    def search(self, documents: list[DocumentRequest]) -> list[DocumentResponse]:
        return self.index_repo.search(documents)
