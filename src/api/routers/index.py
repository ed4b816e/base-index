from fastapi import APIRouter
from src.schemas.response import ResponseSchema
from src.schemas.document_response import DocumentsResponse
from src.schemas.document_request import DocumentsRequest
from src.api.dependencies.services import FaissIndex

router = APIRouter(prefix="", tags=["Index"])


@router.post("/search", response_model=DocumentsResponse, response_model_exclude_none=True)
async def search_endpoint(request: DocumentsRequest, index: FaissIndex):
    processed_documents = index.search(request.documents)
    return {"documents": processed_documents}


@router.post("/fit", response_model=ResponseSchema, response_model_exclude_none=True)
async def fit_endpoint(request: DocumentsRequest, index: FaissIndex):
    index.fit(request.documents)
    return {"success": True, "message": "Documents fitted successfully."}