from fastapi import APIRouter

from src.api.routers.index import router as index_router
from src.constants import PREFIX


router = APIRouter(prefix=PREFIX)

routers = [
    index_router
]

for router_ in routers:
    router.include_router(router_)
