from typing import Annotated
from fastapi import Depends
from src.services.faiss_index import IndexService
from src.repositories.faiss_index import FaissIndexRepository


FaissIndex = Annotated[IndexService, Depends(lambda: IndexService(FaissIndexRepository()))]
