from os import getenv


PREFIX = getenv("PREFIX", "/base-index")

KEYSPACE = getenv("KEYSPACE", "base_index")
