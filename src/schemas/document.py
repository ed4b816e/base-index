import datetime
from typing import Iterable

from pydantic import BaseModel


class DocumentSchema(BaseModel):
    id: str
    date: datetime.datetime
    domain: str

    def __hash__(self) -> int:
        return hash((self.id, self.date))


class DocumentsSchema(BaseModel):
    documents: list[DocumentSchema]

    def __iter__(self) -> Iterable[DocumentSchema]:
        yield from self.documents
