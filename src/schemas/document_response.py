from .document import DocumentSchema
from .document import DocumentsSchema
from .response import ResponseSchema
from .semantic_from import SemanticFrom


class DocumentResponse(DocumentSchema, ResponseSchema):
    semantic_from: list[SemanticFrom]


class DocumentsResponse(DocumentsSchema, ResponseSchema):
    documents: list[DocumentResponse]
