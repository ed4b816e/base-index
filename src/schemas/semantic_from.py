from pydantic import BaseModel
from pydantic import field_serializer
from pydantic import field_validator


class SemanticFrom(BaseModel):
    id: str
    ratio: float
    delta: int
    domain: str

    @field_validator("delta", mode="before")
    def delta_validator(cls, value) -> int:
        return int(value)

    @field_serializer("ratio")
    def ratio_serialization(self, value: float, info_) -> float:
        return min(round(float(value), 3), 1.0)
