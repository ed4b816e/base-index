from .document import DocumentSchema, DocumentsSchema


class DocumentRequest(DocumentSchema):
    paragraphs_embedding: list[list[float]]
    paragraphs_num: list[int]


class DocumentsRequest(DocumentsSchema):
    documents: list[DocumentRequest]
