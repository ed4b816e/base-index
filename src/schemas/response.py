from pydantic import BaseModel


class ResponseSchema(BaseModel):
    success: bool = True
    message: str | None = None
