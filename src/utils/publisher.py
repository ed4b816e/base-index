import asyncio
import json
import uuid
from asyncio.exceptions import IncompleteReadError
from logging import getLogger

from aio_pika import connect_robust
from aio_pika import DeliveryMode
from aio_pika import ExchangeType
from aio_pika import Message
from aio_pika import RobustChannel
from aio_pika import RobustConnection
from aio_pika import RobustExchange
from aio_pika import RobustQueue
from aio_pika.abc import AbstractIncomingMessage
from aiormq.exceptions import Basic
from aiormq.exceptions import DeliveryError
from aiormq.exceptions import IncompatibleProtocolError

from src.settings import rabbitmq_settings
from src.settings import RabbitMQSettings

logger = getLogger(__file__)


class Publisher:
    class SetEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, set):
                return list(obj)
            return json.JSONEncoder.default(self, obj)

    def __init__(self):
        self.futures = {}
        self.settings: RabbitMQSettings = rabbitmq_settings
        self.connection: RobustConnection = ...
        self.channel: RobustChannel = ...
        self.exchange: RobustExchange = ...
        self.callback_queue: RobustQueue = ...
        self.loop = ...

    async def setup(self, loop):
        self.loop = loop
        try:
            self.connection = await connect_robust(self.settings.url(), loop=loop)
        except (IncompleteReadError, IncompatibleProtocolError) as error:
            logger.warning(error, exc_info=True)
            return
        self.channel = await self.connection.channel()
        self.exchange = await self.channel.declare_exchange(
            name=self.settings.exchange,
            type=ExchangeType.DIRECT,
            auto_delete=False,
            durable=True,
        )
        self.callback_queue = await self.channel.declare_queue(
            exclusive=False, auto_delete=True, durable=False
        )
        await self.callback_queue.consume(self.on_response, no_ack=True)

    async def on_response(self, message: AbstractIncomingMessage) -> None:
        if message.correlation_id is None:
            print(f"Bad message {message!r}")
            return

        try:
            future: asyncio.Future = self.futures.pop(message.correlation_id)
            future.set_result(message.body)
        except KeyError:
            pass

    async def publish(self, message):
        try:
            correlation_id = str(uuid.uuid4())
            future = self.loop.create_future()
            self.futures[correlation_id] = future

            await self.exchange.publish(
                Message(
                    body=json.dumps(
                        message, indent=4, ensure_ascii=False, cls=Publisher.SetEncoder
                    ).encode(),
                    delivery_mode=DeliveryMode.PERSISTENT,
                    correlation_id=correlation_id,
                    reply_to=self.callback_queue.name,
                ),
                routing_key=self.settings.routing_key,
            )
            return await future
        except DeliveryError as error:
            _, command = error.args
            if isinstance(command, Basic.Nack):
                raise Exception("The queue is full.")

    async def payoff(self):
        await self.connection.close()
