import json
import uuid
from asyncio.exceptions import IncompleteReadError
from logging import getLogger

from aio_pika import connect_robust
from aio_pika import Message
from aio_pika import RobustChannel
from aio_pika import RobustConnection
from aio_pika import RobustExchange
from aio_pika import RobustQueue
from aio_pika.abc import AbstractIncomingMessage
from aiormq.exceptions import IncompatibleProtocolError

from src.settings import rabbitmq_settings
from src.settings import RabbitMQSettings

logger = getLogger(__file__)


class Consumer:
    def __init__(self):
        self.queue_uuid = str(uuid.uuid4())
        self.settings: RabbitMQSettings = rabbitmq_settings
        self.connection: RobustConnection = ...
        self.channel: RobustChannel = ...
        self.exchange: RobustExchange = ...
        self.queue: RobustQueue = ...

        self.queue_arguments = {
            "x-max-length": self.settings.max_length,
            "x-overflow": "reject-publish",
            "x-queue-type": "quorum",
            "x-expires": 10000,  # ms
        }

    async def setup(self, loop):
        try:
            self.connection = await connect_robust(self.settings.url(), loop=loop)
        except (IncompleteReadError, IncompatibleProtocolError) as error:
            logger.warning(error, exc_info=True)
            return
        self.channel = await self.connection.channel()
        await self.channel.set_qos(prefetch_count=1)

        self.queue = await self.channel.declare_queue(
            "-".join((self.settings.routing_key, self.queue_uuid)),
            auto_delete=False,
            durable=True,
            arguments=self.queue_arguments,
        )
        self.exchange = self.channel.get_exchange(name=self.settings.exchange)
        await self.queue.bind(self.settings.exchange, routing_key=self.settings.routing_key)

    async def run(self):
        async with self.queue.iterator() as queue_iter:
            message: AbstractIncomingMessage
            async for message in queue_iter:
                async with message.process(requeue=False):
                    assert message.reply_to is not None
                    yield message

    async def send_acknowledgment(self, message: dict, reply_to: str, correlation_id):
        await self.channel.default_exchange.publish(
            Message(
                body=json.dumps(message, indent=4, ensure_ascii=False).encode(),
                correlation_id=correlation_id,
            ),
            routing_key=reply_to,
        )

    async def payoff(self):
        await self.connection.close()
