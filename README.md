# Base Index

Базовый сервис индекса документов, содержащий методы:

/search

Для реализации своей логики взаимодействия с данными можно переписать репозиторий индекса либо написать свой репозиторий и создать аннотированный сервис

Индекс пополняется забираю данные из очереди RabbitMQ

# Ссылки

| Source             | Link                                                     |
|:-------------------|:---------------------------------------------------------|
| RabbitMQ dashboard | <http://localhost:15672/>                                |
| RabbitMQ broker    | <ampq://localhost:5672//>                                |
| Webserver docs     | <http://localhost:8000/docs>                             |
| Webserver redoc    | <http://localhost:8000/redoc>                            |


# Переменные окружения

### Переменные СУБД Cassandra
```shell
# .env
CASSANDRA_HOST=localhost
CASSANDRA_PORT=9042
CASSANDRA_USERNAME=cassandra
CASSANDRA_PASSWORD=cassandra

KEYSPACE=base_index
```
### Переменные индекса
```shell
# .env
INDEX_WINDOW_DAYS=30
INDEX_DIM=768
INDEX_RETURN_TOP_N=10
INDEX_THRESHOLD=0.8
```
### Переменные RabbitMQ
```shell
# .env
RABBITMQ_HOST=localhost
RABBITMQ_PORT=5672
RABBITMQ_USERNAME=guest
RABBITMQ_PASSWORD=guest
RABBITMQ_VIRTUALHOST=/
RABBITMQ_ROUTING_KEY=queue

EXCHANGE=base_index.default.exchange
```
